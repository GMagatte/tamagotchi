package tamagotchi;

import tamagotchi.Tama;
import java.util.Scanner;

public class Interac {

    public void interaction() {

        var scanner = new Scanner(System.in);
        System.out.println("Give it a name : ");
        String name = scanner.nextLine();
        System.out.println("Okay it's name is now " + name);

        System.out.println();

        Tama tama = new Tama();
        tama.getMood();
        tama.passTime();

        while (true) {
            String feed = "Feed";
            String walk = "Walk";
            String pet = "Pet";
            String clean = "Clean";

            System.out.println("What do you want to do ? \n\n" + feed + " \n" + walk + " \n" + pet + " \n" + clean);
            System.out.println();
            String choice = scanner.nextLine();

            if (choice.contains(feed)) {
                while (true) {

                    String cookie = "Cookie";
                    String berries = "Berries";
                    String banana = "Banana";
                    String hotDog = "Hot Dog";
                    String burger = "Burger";

                    System.out.println();
                    System.out.println("What do you want to feed it? \n Choose your food : \n\n" + cookie + "\n"
                            + berries + "\n" + banana + "\n" + hotDog + "\n" + burger);
                    String chooseFood = scanner.nextLine();
                    System.out.println("Your " + name + " gained 5 points of hunger");
                    tama.feed();
                    break;
                }

            }



            if (choice.contains(walk)) {
                while (true) {

                    String parc = "Park";
                    String mall = "Mall";
                    String garden = "Flower Garden";

                    System.out.println();
                    System.out.println(name + " is so happy to go for a walk! Where do you want to go? : \n\n" + parc+ "\n" + mall + "\n" + garden);
                    String toGo = scanner.nextLine();
                    System.out.println();

                    if (toGo.equals(parc)) {
                        System.out.println(name+ " loves the park and gained 3 pts of happiness");
                    } 
                    
                    if (toGo.equals(mall)) {
                        System.out.println(name+ " would love to get some new toys from ToysForTama at the mall and gained 3 pts of happiness");
                    }
                    
                    if (toGo.equals(garden)) {
                        System.out.println(name+ " don't really like flowers because they are full of bees but he still happy to be with and gained 3 pts of happiness");
                    }
                    System.out.println();
                    tama.walk();
                    tama.clean();
                    break;
                    
                }

            }



            if (choice.contains(pet)) {
                while (true) {

                    String caresse = "Pet";
                    String play = "Play";
                    String calin = "Cuddle";

                    System.out.println();
                    System.out.println("Your "+ name+" wants attention \n\n" + caresse+ "\n" +play+ "\n" + calin);
                    String playWith = scanner.nextLine();
                    System.out.println();

                    if (playWith.equals(caresse)) {
                        System.out.println(name+" gained 5 pts of happiness");
                    }

                    if (playWith.equals(play)) {
                        System.out.println(name+ " gained 5 pts of happiness.. But he is getting a little dirty too!");
                        tama.clean();
                    }

                    if (playWith.equals(calin)) {
                        System.out.println(name+ " loves cuddling with you and gained 5 pts of happiness");
                    }
                    tama.pet();
                    break;
 
                }
                
            }
            

            if (choice.contains(clean)) {
                while (true) {

                    String bath = "Bath";
                    String poop = "Poop";

                    System.out.println();
                    System.out.println("Your "+name+" feel uncomfortable... \n Try cleaning it : \n\n"+ bath+ "\n"+poop);
                    String poopOrNot = scanner.nextLine();
                    System.out.println();

                    if (poopOrNot.equals(bath)) {
                        System.out.println(name+" is now super clean and ready to play again");
                    }

                     if (poopOrNot.equals(poop)) {
                        System.out.println(name +" feels better and wants more cookies");
                    }

                    tama.clean();
                    break;
                }

            }

        }

    }

}
