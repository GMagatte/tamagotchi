package tamagotchi;

import java.util.Scanner;

public class Tama {

   public String name;
   private int lifeState = 10;
   private int hunger  = 10;
   private int dirtiness = 10;
   private int happiness = 10;
   private int tiredness = 0;

  


    public void feed() {
        
        hunger += 5;

        if (hunger < 0){
            hunger = 0;
        }

        if (hunger > 20) {
            hunger = 20;
        }
        
    }


    public void walk() {

        happiness += 3;
        tiredness += 2;
        
        if (happiness > 20) {
            happiness = 20;
        }

        if (tiredness > 10) {
            tiredness = 10;
        }

    }



    public void clean() {
        dirtiness -= 10;

        if (dirtiness < 0) {
            dirtiness = 0;
        }

    }



    public void pet() {

        happiness += 5;

        if (happiness > 20) {
            happiness = 20;
        }

    }


    public String getMood() {
        
        if (tiredness == 10) {
            return "asleep";
        } else if (tiredness >= 8) {
            return "tired";
        } else if (hunger >= 10) {
            return "hungry";
        } else if (dirtiness > 4) {
            return "dirty";
        } else if (happiness >= 15) {
            return "happy";
        } else if (happiness >= 10) {
            return "normal";
        } return "sad";

    }
    


    public void passTime() {

        hunger -= 5;
        dirtiness += 5;
        tiredness += 5;

        if (hunger == 0) {
            lifeState -= 5; 
        }

         if (dirtiness > 10) {
            dirtiness = 10;
        }

        if (lifeState == 0) {
            System.out.println("Your Tamagotchi is dead... Shame on you!");
        }

       
    }

    
}
